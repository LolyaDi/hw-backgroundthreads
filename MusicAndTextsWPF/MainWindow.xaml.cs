﻿using System;
using System.ComponentModel;
using System.IO;
using System.Threading;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Threading;

namespace MusicAndTextsWPF
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private bool _isPlaying, _isDraggingSlider;

        public MainWindow()
        {
            InitializeComponent();

            _isPlaying = _isDraggingSlider = false;

            audioPlayer.Source = new Uri("EscapeTheFate-IAmHuman.mp3", UriKind.RelativeOrAbsolute);

            var timer = new DispatcherTimer
            {
                Interval = TimeSpan.FromSeconds(1)
            };

            timer.Tick += (sender, e) =>
            {
                if (audioPlayer.NaturalDuration.HasTimeSpan && !_isDraggingSlider)
                {
                    progressSlider.Minimum = 0;
                    progressSlider.Maximum = audioPlayer.NaturalDuration.TimeSpan.TotalSeconds;
                    progressSlider.Value = audioPlayer.Position.TotalSeconds;
                }
            };

            timer.Start();
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            var textSavingThread = new Thread(SaveTextEntries);
            textSavingThread.Start();
        }

        private void PlayButtonClick(object sender, RoutedEventArgs e)
        {
            ThreadPool.QueueUserWorkItem((state) =>
            {
                Dispatcher.BeginInvoke(new Action(() =>
                {
                    if (!_isPlaying)
                    {
                        audioPlayer.Play();
                        _isPlaying = true;

                        progressSlider.IsEnabled = true;

                        buttonImage.Source = new ImageSourceConverter().ConvertFromString("https://image.flaticon.com/icons/png/512/183/183668.png") as ImageSource;
                    }
                    else
                    {
                        audioPlayer.Pause();
                        _isPlaying = false;

                        buttonImage.Source = new ImageSourceConverter().ConvertFromString("https://static.thenounproject.com/png/921406-200.png") as ImageSource;
                    }
                }), DispatcherPriority.Background);
            }, null);
        }

        private void ProgressSliderDragStarted(object sender, System.Windows.Controls.Primitives.DragStartedEventArgs e)
        {
            _isDraggingSlider = true;
        }

        private void ProgressSliderDragCompleted(object sender, System.Windows.Controls.Primitives.DragCompletedEventArgs e)
        {
            _isDraggingSlider = false;
            audioPlayer.Position = TimeSpan.FromSeconds(progressSlider.Value);
        }

        private void ProgressSliderValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            progressTime.Text = TimeSpan.FromSeconds(progressSlider.Value).ToString(@"hh\:mm\:ss");
        }

        private void SaveTextEntries()
        {
            string textEntries = new TextRange(textEntriesBox.Document.ContentStart, textEntriesBox.Document.ContentEnd).Text;
            using (var stream = new StreamWriter(@".\Text.txt", false))
            {
                stream.Write(textEntries);
            }
        }
    }
}
